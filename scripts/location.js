import { getUniqueList, getMinMax } from './utils';
import * as d3 from 'd3';
import * as d3Cloud from 'd3-cloud';
import SunburstChart from 'sunburst-chart';
import _ from 'lodash';

function Location(data, { height, width, margin, chartTooltip, chartTransitionDuration }) {
    const scaleOrdinal = d3.scaleOrdinal(d3.schemeCategory10);

    const universityChartProps = buildCloudChart('#university-cloud-chart');
    universityChartProps.scaleOrdinal = scaleOrdinal;
    universityChartProps.colors = [];

    const locationFeeChartProps = buildSunburstChart('location-fee-chart', {
        scaleOrdinal,
        value: 'fee',
    });
    const locationEnrollmentChartProps = buildSunburstChart('location-enrollment-chart', {
        scaleOrdinal,
        value: 'enrollment',
    });

    drawCloudChart(data, universityChartProps);

    const sunburstStateData = _.groupBy(data, 'state');
    drawSunburstChart(sunburstStateData, locationFeeChartProps);
    drawSunburstChart(sunburstStateData, locationEnrollmentChartProps);

    // START Function definitions

    this.update = function (newData) {
        drawCloudChart(newData, universityChartProps);

        const sunburstStateData = _.groupBy(newData, 'state');
        drawSunburstChart(sunburstStateData, locationFeeChartProps);
        drawSunburstChart(sunburstStateData, locationEnrollmentChartProps);
    };

    function buildCloudChart(selector) {
        const svg = d3.select(selector).append('svg').attr('viewBox', [0, 0, width, height]).attr('text-anchor', 'middle');

        // NOTE:: if using on('word', () => {}), remove below attr()
        const g = svg.append('g').attr('transform', `translate(${width / 2}, ${height / 2})`);

        const cloud = d3Cloud()
            .size([width, height])
            .padding(2)
            .fontSize((d) => d.font_size)
            .fontWeight((d) => d.font_weight)
            .rotate(0)
            .text((d) => d.text)
            .on('end', (words) => {
                g.selectAll('text').remove();

                g.selectAll('text')
                    .data(words)
                    .enter()
                    .append('text')
                    .attr('fill', (d) => d.fill)
                    .attr('font-size', (d) => d.size)
                    .attr('font-weight', (d) => d.weight)
                    .attr('transform', 'translate(0,0)rotate(0)')
                    .text((d) => d.text)
                    .transition()
                    .duration((words.length * chartTransitionDuration) / 1.5 / 2)
                    .attr('transform', (d) => `translate(${d.x}, ${d.y})rotate(${d.rotate})`);
            });
        // .on('word', ({ fill, font, rotate, size, text, weight, x, y }) => {
        //     g.append('text')
        //         .attr('fill', fill)
        //         .attr('font-size', size)
        //         .attr('font-weight', weight)
        //         .attr('transform', `translate(${x}, ${y})rotate(${rotate})`)
        //         .text(text);
        // })

        return { cloud };
    }

    function drawCloudChart(chartData, { cloud, scaleOrdinal, colors }) {
        let cloudData = [];
        let fontSizes = [];
        const { min: MIN_RANK, max: MAX_RANK } = getMinMax(getUniqueList(chartData, 'rank'));

        chartData.forEach((row) => {
            const isMinRank = row.rank === MIN_RANK;

            const { name, rank } = row;
            let fill = colors[rank];
            if (fill === undefined) {
                fill = colors[rank] = scaleOrdinal(rank);
            }

            let fontSize = fontSizes[rank];
            if (fontSize === undefined) {
                let tmpFontSize = Math.round(((MAX_RANK - rank) % MAX_RANK) / 10 + 15);

                if (isMinRank) {
                    tmpFontSize += 7;
                }

                while (fontSizes.includes(tmpFontSize)) {
                    tmpFontSize -= 3;
                    if (tmpFontSize < 0) {
                        tmpFontSize = 0;
                        break;
                    }
                }

                fontSize = fontSizes[rank] = tmpFontSize;
            }

            cloudData.push({
                text: name,
                font_size: fontSize,
                fill: fill,
                font_weight: isMinRank ? 'bold' : 'normal',
            });
        });

        cloud.words(cloudData.filter((row) => row.font_size !== 0)).start();
    }

    function buildSunburstChart(selectorId, { scaleOrdinal, value }) {
        const chart = SunburstChart()(document.getElementById(selectorId))
            .height(height + 200)
            .width(width)
            .label('name')
            .size(value)
            .color((d, parent) => scaleOrdinal(parent ? parent.data.name : null))
            .tooltipContent((d, node) => `${_.upperFirst(value)}: <i>${node.value}</i>`)
            .transitionDuration(chartTransitionDuration * 10);

        return { chart };
    }

    function drawSunburstChart(chartData, { chart }) {
        chart.data({
            name: 'State',
            children: _.map(chartData, (stateChildren, stateName) => {
                return {
                    name: stateName,
                    children: stateChildren,
                };
            }),
        });
    }
}

export default Location;
