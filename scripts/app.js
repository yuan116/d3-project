import { getJsonData, getUniqueList, getMinMax, filterData } from './utils';
import 'bootstrap';
import bootstrapSlider from 'bootstrap-slider';
import * as d3 from 'd3';
import Location from './location';
import Rank from './rank';

window.jQuery = window.$ = $;

window.onload = async () => {
    const DEV_YEAR = 2021;
    const currentYear = new Date().getFullYear();
    if (DEV_YEAR !== currentYear) {
        $('#copyright-year').html(`- ${currentYear}`);
    }

    const jsonData = await getJsonData().catch((reason) => {
        window.alert(reason);
    });

    if (jsonData === undefined) {
        $('#filter-btn').remove();
        $('.app-slider-container').remove();
        return;
    }

    $('#footer-container').removeClass('fixed-bottom');

    const { min: MIN_RANK, max: MAX_RANK } = getMinMax(
        getUniqueList(jsonData, 'rank')
    );
    const rankBootsrapSlider = new bootstrapSlider('#rank-slider', {
        min: MIN_RANK,
        max: MAX_RANK,
        value: [MIN_RANK, MAX_RANK],
        tooltip: 'always',
    });
    $('#rank-slider-spinner').remove();

    const { min: MIN_FEE, max: MAX_FEE } = getMinMax(
        getUniqueList(jsonData, 'fee')
    );
    const feeBootstrapSlider = new bootstrapSlider('#fee-slider', {
        min: MIN_FEE,
        max: MAX_FEE,
        value: [MIN_FEE, MAX_FEE],
        tooltip: 'always',
    });
    $('#fee-slider-spinner').remove();

    $('.slider.slider-horizontal').addClass('w-100');

    $('#state-checkbox-container').empty();
    getUniqueList(jsonData, 'state').forEach((value) => {
        $('#state-checkbox-container').append(
            `<div class="form-check">
                <input type="checkbox" class="form-check-input app-state-checkbox-input" id="${value}-checkbox" value="${value}" checked />
                <label class="form-check-label" for="${value}-checkbox">
                    ${value}
                </label>
            </div>`
        );
    });

    const chartProps = {
        height: 400,
        width: $('#app-chart-tab-content').width(),
        margin: {
            top: 10,
            right: 30,
            bottom: 20,
            left: 40,
        },
        chartTooltip: d3
            .select('body')
            .append('div')
            .attr(
                'class',
                'app-chart-tooltip position-absolute text-center p-1 bg-white border border-dark rounded'
            )
            .style('opacity', 0),
        chartTransitionDuration: 40,
    };

    const rank = new Rank(Rank.finalizeData(jsonData), chartProps);
    const location = new Location(jsonData, chartProps);

    $(function () {
        $('[data-bs-toggle="tooltip"]').tooltip();

        $('#filter-btn').on('click', async function () {
            const filteredData = await filterData(jsonData, {
                rankSlider: rankBootsrapSlider,
                feeSlider: feeBootstrapSlider,
            });

            location.update(filteredData);
            rank.update(Rank.finalizeData(filteredData));
        });
    });
};
