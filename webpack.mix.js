const mix = require('laravel-mix');

mix.setPublicPath('static')
    .copyDirectory(
        'node_modules/@fortawesome/fontawesome-free/webfonts',
        'static/fonts'
    )
    .sass('styles/_app.scss', 'css/app.css')
    .sass('styles/_vendor.scss', 'css/vendor.css', {
        processUrls: false,
    })
    .sourceMaps(false)
    .autoload({
        jquery: ['$', 'jQuery'],
        lodash: '_',
    })
    .js('scripts/app.js', 'js/app.js')
    .extract(['jquery', 'lodash'], 'js/vendor-bundle1.js')
    .extract(['bootstrap', 'bootstrap-slider'], 'js/vendor-bundle2.js')
    .extract()
    .minify([
        'static/css/app.css',
        'static/css/vendor.css',
        'static/js/app.js',
        'static/js/manifest.js',
        'static/js/vendor.js',
        'static/js/vendor-bundle1.js',
        'static/js/vendor-bundle2.js',
    ]);

// mix.webpackConfig({
//     module: {
//         rules: [
//             {
//                 test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
//                 use: [{
//                     loader: 'file-loader',
//                     options: {
//                         name: '[name].[ext]',
//                         outputPath: 'fonts',
//                     }
//                 }]
//             },
//         ]
//     }
// });

if (mix.inProduction()) {
    mix.version();
}
