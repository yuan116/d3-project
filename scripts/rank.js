import * as d3 from 'd3';

function Rank(data, { height, width, margin, chartTooltip, chartTransitionDuration }) {
    const feeChartProps = buildLineChart('#fee-line-chart', data, {
        xDomainCallback: (data) => data.rank,
        xCallbackKey: 'rank',
        xDomainLabel: 'Rank',
        yDomainCallback: (data) => data.average_fee,
        yCallbackKey: 'average_fee',
        yDomainLabel: 'Average Fees',
    });
    feeChartProps.tooltipKey = 'hover_fee_text';

    const enrollmentChartProps = buildLineChart('#enrollment-line-chart', data, {
        xDomainCallback: (data) => data.rank,
        xCallbackKey: 'rank',
        xDomainLabel: 'Rank',
        yDomainCallback: (data) => data.average_enrollment,
        yCallbackKey: 'average_enrollment',
        yDomainLabel: 'Average Enrollments',
    });
    enrollmentChartProps.tooltipKey = 'hover_enrollment_text';

    drawLineChart(data, feeChartProps);
    drawLineChart(data, enrollmentChartProps);

    // START Function definitions

    this.update = function (newData) {
        drawLineChart(newData, feeChartProps);
        drawLineChart(newData, enrollmentChartProps);
    };

    function buildLineChart(
        selector,
        chartData,
        {
            xDomainCallback = (data) => data.x,
            xCallbackKey = 'x',
            xDomainLabel = 'X-Axis Label',
            yDomainCallback = (data) => data.y,
            yCallbackKey = 'y',
            yDomainLabel = 'Y-Axis Label',
        }
    ) {
        const svg = d3.select(selector).append('svg').attr('viewBox', [0, 0, width, height]);

        //X-axis
        const xAxisCallback = d3
            .scaleLinear()
            .domain([0, d3.max(chartData, xDomainCallback)])
            .nice()
            .range([margin.left, width - margin.right]);

        //X-axis Bar
        svg.append('g').call((g) =>
            g
                .attr('transform', `translate(0, ${height - margin.bottom})`)
                .call(
                    d3
                        .axisBottom(xAxisCallback)
                        .ticks(width / 80)
                        .tickSizeOuter(0)
                )
                .call((g) =>
                    g.select('.tick:last-of-type text').clone().attr('y', -10).attr('text-anchor', 'start').attr('font-weight', 'bold').text(xDomainLabel)
                )
        );

        //Y-axis
        const yAxisCallback = d3
            .scaleLinear()
            .domain([0, d3.max(chartData, yDomainCallback)])
            .nice()
            .range([height - margin.bottom, margin.top]);

        //Y-axis Bar
        svg.append('g').call((g) =>
            g
                .attr('transform', `translate(${margin.left}, 0)`)
                .call(d3.axisLeft(yAxisCallback))
                .call((g) =>
                    g.select('.tick:last-of-type text').clone().attr('x', 3).attr('text-anchor', 'start').attr('font-weight', 'bold').text(yDomainLabel)
                )
        );

        return {
            xCallback: (data) => xAxisCallback(data[xCallbackKey]),
            yCallback: (data) => yAxisCallback(data[yCallbackKey]),
            svg,
        };
    }

    function drawLineChart(chartData, { xCallback, yCallback, svg, tooltipKey = 'tooltip' }) {
        svg.select(`path#${tooltipKey}-path`).remove();

        const path = svg
            .append('path')
            .attr('id', `${tooltipKey}-path`)
            .attr('fill', 'none')
            .attr('stroke', 'steelblue')
            .attr('stroke-width', 1.5)
            .attr('stroke-linejoin', 'round')
            .attr('stroke-linecap', 'round');

        path.datum(chartData).attr('d', d3.line().x(xCallback).y(yCallback));

        const totalLength = path.node().getTotalLength();

        path.attr('stroke-dasharray', totalLength)
            .attr('stroke-dashoffset', totalLength)
            .transition()
            .duration(chartData.length * chartTransitionDuration)
            .ease(d3.easeLinear)
            .attr('stroke-dashoffset', 0);

        svg.selectAll('circle').remove();

        svg.selectAll('circle')
            .data(chartData)
            .enter()
            .append('circle')
            .attr('r', 3)
            .attr('cx', xCallback)
            .attr('cy', yCallback)
            .on('mouseover', (event, data) =>
                chartTooltip
                    .style('opacity', 1)
                    .html(data[tooltipKey])
                    .style('left', `${event.pageX}px`)
                    .style('top', `${event.pageY - 28}px`)
            )
            .on('mouseout', (event) => chartTooltip.style('opacity', 0));
    }

    // END Function definitions
}

Rank.finalizeData = (filteredData) => {
    let finalizedData = [];

    filteredData.forEach((row) => {
        const foundRank = Boolean(finalizedData.find((finalizeRow) => finalizeRow.rank === row.rank));

        if (!foundRank) {
            const tmpList = {
                rank: row.rank,
                average_fee: 0,
                hover_fee_text: '',
                average_enrollment: 0,
                hover_enrollment_text: '',
            };

            let totalFees = 0;
            let hoverFees = [];
            let totalEnrollments = 0;
            let hoverEnrollments = [];

            const filteredRankData = filteredData.filter((tmpRow) => tmpRow.rank === row.rank);

            filteredRankData.forEach((filteredRow) => {
                totalFees += filteredRow.fee;
                hoverFees.push(`${filteredRow.name} : ${filteredRow.fee} fees`);

                totalEnrollments += filteredRow.enrollment;
                hoverEnrollments.push(`${filteredRow.name} : ${filteredRow.enrollment} enrollments`);
            });

            tmpList.average_fee = Math.ceil(totalFees / filteredRankData.length);
            hoverFees.push(`Average : ${tmpList.average_fee} fees`);
            tmpList.hover_fee_text = hoverFees.join('<br>');

            tmpList.average_enrollment = Math.ceil(totalEnrollments / filteredRankData.length);
            hoverEnrollments.push(`Average : ${tmpList.average_enrollment} enrollments`);
            tmpList.hover_enrollment_text = hoverEnrollments.join('<br>');

            finalizedData.push(tmpList);
        }
    });

    return finalizedData;
};

export default Rank;
