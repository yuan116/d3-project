function getJsonData() {
    return new Promise((resolve, reject) => {
        $.ajax({
            method: 'GET',
            url: 'static/data/data.json',
            dataType: 'json',
        })
            .done((data, textStatus, jqXHR) => {
                resolve(data);
            })
            .fail((jqXHR, textStatus, errorThrown) => {
                reject('Fail to fetch data.');
            });
    });
}

function getUniqueList(data, key) {
    return _.uniq(data.map((value) => value[key]).sort());
}

function getMinMax(data) {
    return {
        min: Math.min(...data),
        max: Math.max(...data),
    };
}

function filterData(data, { rankSlider, feeSlider }) {
    const { 0: RANK_MIN_VALUE, 1: RANK_MAX_VALUE } = rankSlider.getValue();
    const { 0: FEE_MIN_VALUE, 1: FEE_MAX_VALUE } = feeSlider.getValue();

    let stateCheckedList = [];
    $('.app-state-checkbox-input').each((index, element) => {
        if ($(element).is(':checked')) {
            stateCheckedList.push(element.value.trim());
        }
    });

    return data.filter(
        (row) =>
            row.rank >= RANK_MIN_VALUE &&
            row.rank <= RANK_MAX_VALUE &&
            row.fee >= FEE_MIN_VALUE &&
            row.fee <= FEE_MAX_VALUE &&
            stateCheckedList.includes(row.state)
    );
}

export { getJsonData, getUniqueList, getMinMax, filterData };
